# My Serial List

## TEMP
Usage:

`$ python3 venv -m venv`

`$ source venv/bin/activate`

`$ pip install -r requirements.txt`

or

`$ python3 bootstrap.py`

`$ source venv/bin/activate`

## Documentation

- [Auth HTTP methods](doc/http-auth-methods.md)