import os

basedir = os.path.abspath(os.path.dirname(__file__))
POSTGRES = {
    'user': '',
    'pw': '',
    'db': '',
    'host': '',
    'port': '5432',
}


class BaseConfig:
    SECRET_KEY = os.getenv('SECRET_KEY', 'key')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = \
        'postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' \
        % POSTGRES


class DevelopmentConfig(BaseConfig):
    DEBUG = True


class TestingConfig(BaseConfig):
    DEBUG = True


class TestingAdminUser:
    USER = 'admin'
    PASSWORD = 'admin'
    FIRST_NAME = 'admin'
    SECOND_NAME = 'admin'
    ROLE = 'admin'


class TestingUser:
    USER = 'example_user'
    PASSWORD = '123456'
    FIRST_NAME = 'example'
    SECOND_NAME = 'user'
    ROLE = 'user'


class ProductionConfig(BaseConfig):
    DEBUG = True
