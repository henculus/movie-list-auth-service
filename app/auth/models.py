import datetime

from app import db, bcrypt, app


class User(db.Model):
    """ User Model for storing user related details """
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    first_name = db.Column(db.String(255), nullable=False)
    second_name = db.Column(db.String(255), nullable=False)
    user = db.Column(db.String(255), nullable=False)
    password = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False, unique=True)
    registered_on = db.Column(db.DateTime, nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)

    def __init__(self, first_name, second_name, user, password, email, admin=False):
        self.first_name = first_name
        self.second_name = second_name
        self.user = user
        self.password = bcrypt.generate_password_hash(
            password, app.config.get('BCRYPT_LOG_ROUNDS')
        ).decode()
        self.admin = admin
        self.email = email
        self.registered_on = datetime.datetime.now()

    def as_dict(self):
        return {'first_name': self.first_name,
                'second_name': self.second_name,
                'user': self.user,
                'password': self.password,
                'e_mail': self.email,
                'admin': self.admin,
                'registered_on': self.registered_on}
