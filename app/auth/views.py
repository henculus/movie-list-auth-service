from flask import Blueprint, make_response, jsonify, request
from flask.views import MethodView
from app import db
from app.auth.models import User

auth_blueprint = Blueprint('auth', __name__)


class RegisterAPI(MethodView):
    @staticmethod
    def get():
        user = User('test_fn',
                    'test_sn',
                    'test',
                    '123',
                    email=request.args.get('email'))
        db.session.add(user)
        db.session.commit()

        response = {
            'result': 'success'
        }

        return make_response(jsonify(response)), 200

    @staticmethod
    def post():
        post_data  = request.get_json()
        user = User(
            'test_fn',
            'test_sn',
            'test',
            '123',
            email=post_data.get('email')
        )

        db.session.add(user)
        db.session.commit()

        response = {
            'result': 'success'
        }

        return make_response(jsonify(response)), 200


registration_view = RegisterAPI.as_view('register_api')

auth_blueprint.add_url_rule(
    '/register',
    view_func=registration_view,
    methods=['GET', 'POST']

)
