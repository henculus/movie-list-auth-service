from tests.base_test_case import BaseTestCase
from app.config import TestingUser
from tests.http_methods import register_user

# TODO: Need to resolve case with models


class TestRegistration(BaseTestCase):

    def test_registration(self):
        data = register_user(self,
                             'new_user',
                             'new_user_password',
                             'new',
                             'user')
        self.assertTrue(data['access_token'] != 0)
        self.assertTrue(data['refresh_token'] != 0)

    def test_registration_with_already_registered_user(self):
        data = register_user(self,
                             TestingUser.USER,
                             TestingUser.PASSWORD,
                             TestingUser.FIRST_NAME,
                             TestingUser.SECOND_NAME)
        self.assertTrue(data['error'] == 'User with this username is already '
                                         'registered')
