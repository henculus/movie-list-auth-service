from app.config import TestingUser
from tests.base_test_case import BaseTestCase
from tests.http_methods import get_info_post


class TestGetInfoPost(BaseTestCase):

    def test_get_info_post(self):
        data = get_info_post(self, TestingUser.USER, self.admin_access_token)
        self.assertTrue(data['username'] == TestingUser.USER)
        self.assertTrue(data['first_name'] == TestingUser.FIRST_NAME)
        self.assertTrue(data['second_name'] == TestingUser.SECOND_NAME)
        self.assertTrue(data['email'] != 0)

    def test_get_info_post_without_login(self):
        data = get_info_post(self, TestingUser.USER, 'invalid_token')
        self.assertTrue(data['error'] == 'You do not have permission to '
                                         'perform this operation')

    def test_get_info_post_without_permission(self):
        data = get_info_post(self, TestingUser.USER, self.user_access_token)
        self.assertTrue(data['error'] == 'You do not have permission to '
                                         'perform this operation')

    def test_get_info_post_about_non_existing_user(self):
        data = get_info_post(self, 'invalid_user', self.admin_access_token)
        self.assertTrue(data['error'] == 'This user does not exists')
