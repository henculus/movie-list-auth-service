import logging

from flask_testing import TestCase

from app import app
from app.config import TestingUser, TestingAdminUser
from tests.http_methods import register_user, login_user


class BaseTestCase(TestCase):
    def create_app(self):
        app.config.from_object('app.config.TestingConfig')
        return app

    def setUp(self):
        user_data = register_user(self,
                                  TestingUser.USER,
                                  TestingUser.PASSWORD,
                                  TestingUser.FIRST_NAME,
                                  TestingUser.SECOND_NAME)
        self.user_access_token = user_data['access_token']
        self.user_refresh_token = user_data['refresh_token']

        admin_data = login_user(self,
                                TestingAdminUser.USER,
                                TestingAdminUser.PASSWORD)
        if admin_data['error'] == 0:
            self.admin_access_token = admin_data['access_token']
            self.admin_refresh_token = admin_data['refresh_token']
        else:
            logging.error('Cannot find testing admin user')

    def tearDown(self):
        pass
