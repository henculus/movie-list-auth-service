from app import app
from tests.base_test_case import BaseTestCase


class TestConfig(BaseTestCase):

    def test_key_is_not_default(self):
        self.assertTrue(app.config['SECRET_KEY'] is not 'key')
