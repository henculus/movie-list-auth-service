import json


def decode(func):
    def wrapper():
        response = func()
        data = json.loads(response.data.decode())
        return data
    return wrapper


@decode
def add_role(self, username, role, access_token):
    return self.app.post('/add_role',
                         data=json.dumps(dict(
                             username=username,
                             role=role
                         )),
                         content_type='application/json',
                         authorization='Bearer ' + access_token)


@decode
def change_password(self, old_password, new_password, access_token):
    return self.app.post("/change_password",
                         data=json.dumps(dict(
                             old_password=old_password,
                             new_password=new_password
                         )),
                         content_type='application/json',
                         authorization='Bearer ' + access_token)


@decode
def delete_role(self, username, role, access_token):
    return self.app.delete("/delete_role",
                           data=json.dumps(dict(
                               username=username,
                               role=role
                           )),
                           content_type='application/json',
                           authorization='Bearer ' + access_token)


@decode
def get_info(self, access_token):
    return self.app.get("/user_info",
                        autorization='Bearer ' + access_token)


@decode
def get_info_post(self, username, access_token):
    return self.app.post("/user_info",
                         data=json.dumps(dict(
                             username=username
                         )),
                         content_type='application/json',
                         authorization='Bearer ' + access_token)


@decode
def logout(self, access_token):
    return self.app.get("/logout", authorization='Bearer ' + access_token)


@decode
def refresh_access_token(self, refresh_token):
    return self.app.get("/refresh", authorization='Bearer ' + refresh_token)


@decode
def register_user(self, username, password, first_name, second_name):
    return self.app.post("/register",
                         data=json.dumps(dict(username=username,
                                              password=password,
                                              first_name=first_name,
                                              second_name=second_name)
                                         ),
                         content_type='application/json')


@decode
def login_user(self, username, password):
    return self.app.post("/login",
                         data=json.dumps(dict(
                             username=username,
                             password=password
                         )),
                         content_type='application/json')
