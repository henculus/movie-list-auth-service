from tests.base_test_case import BaseTestCase
from tests.http_methods import logout


class TestLogout(BaseTestCase):

    def test_logout(self):
        data = logout(self,
                      self.user_access_token)
        self.assertTrue(data['error'] == 0)
        self.assertTrue(data['user_access_token'] == 0)

    def test_logout_when_not_logged_in(self):
        data = logout(self,
                      'invalid_token')
        self.assertTrue(data['error'] == 'You must be logged')
