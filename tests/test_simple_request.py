import json

from flask_testing import TestCase
from app import app


def get_request(self):
    return self.client.get('/register?email=5')


def post_request(self):
    return self.client.post(
        '/register',
        data=json.dumps(dict(
            email='6'
        )),

        content_type='application/json',
    )


class TestRegistration(TestCase):
    def create_app(self):
        app.config.from_object('app.config.ProductionConfig')
        return app

    def test_get_request(self):
        response = get_request(self)
        data = json.loads(response.data.decode())
        self.assertTrue(data['result'] == 'success')

    def test_post_request(self):
        response = post_request(self)
        data = json.loads(response.data.decode())
        self.assertTrue(data['result'] == 'success')

