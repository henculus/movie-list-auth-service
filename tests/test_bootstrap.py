import unittest
import logging

# TODO: Make tests


class BootstrapTestCase(unittest.TestCase):
    def test_bootstrap(self):
        logging.warning("Cannot test this script, because it should create "
                        "environment, but this test will be run inside this "
                        "environment")
        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()
