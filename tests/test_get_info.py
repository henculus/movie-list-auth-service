from app.config import TestingUser
from tests.base_test_case import BaseTestCase
from tests.http_methods import get_info


class TestGetInfo(BaseTestCase):
    def test_get_info(self):
        data = get_info(self, self.user_access_token)
        self.assertTrue(data['username'] == TestingUser.USER)
        self.assertTrue(data['first_name'] == TestingUser.FIRST_NAME)
        self.assertTrue(data['second_name'] == TestingUser.SECOND_NAME)
        self.assertTrue(data['email'] != 0)

    def test_get_info_without_login(self):
        data = get_info(self, 'invalid_token')
        self.assertTrue(data['error' == 'You do not have permission to perform'
                                        'this operation'])
