from app.config import TestingUser
from tests.base_test_case import BaseTestCase
from tests.http_methods import change_password


class TestChangePassword(BaseTestCase):

    def test_change_password(self):
        data = change_password(self,
                               TestingUser.PASSWORD,
                               'new_password',
                               self.user_access_token)
        self.assertTrue(data['error'] == 0)

    def test_change_password_with_wrong_current_password(self):
        data = change_password(self,
                               'wrong_password',
                               'new_password',
                               self.user_access_token)
        self.assertTrue(data['error'] == 'Invalid current password')

    def test_change_password_without_login(self):
        data = change_password(self,
                               TestingUser.PASSWORD,
                               'new_password',
                               'invalid_access_token')
        self.assertTrue(data['error'] == 'You do not have permission to '
                                         'perform this operation')
