from tests.http_methods import login_user
from app.config import TestingUser
from tests.base_test_case import BaseTestCase

# TODO: May be try to call @login-require methods to check token


class TestLogin(BaseTestCase):
    def test_login(self):
        data = login_user(self, TestingUser.USER, TestingUser.PASSWORD)
        self.assertTrue(data['access_token'] != 0)
        self.assertTrue(data['refresh_token'] != 0)

    def test_non_registered_user_login(self):
        data = login_user(self, 'non_registered_user', '123456')
        self.assertTrue(data['error'] == 'User does not exists')

    def test_non_valid_credentials_login(self):
        data = login_user(self, 'example_user', '654321')
        self.assertTrue(data['error'] == 'Invalid password')
