from app.config import TestingUser
from tests.base_test_case import BaseTestCase
from tests.http_methods import delete_role


class TestDeleteRole(BaseTestCase):

    def test_delete_role(self):
        data = delete_role(self,
                           TestingUser.USER,
                           TestingUser.ROLE,
                           self.admin_access_token)
        self.assertTrue(data['error'] == 0)

    def test_delete_non_existing_role(self):
        data = delete_role(self,
                           TestingUser.USER,
                           'invalid_role',
                           self.admin_access_token)
        self.assertTrue(data['error'] == 'This role does not exists')

    def test_delete_role_from_non_existing_user(self):
        data = delete_role(self,
                           'invalid_user',
                           TestingUser.ROLE,
                           self.admin_access_token)
        self.assertTrue(data['error'] == 'This user does not exists')

    def test_delete_role_without_permissions(self):
        data = delete_role(self,
                           TestingUser.USER,
                           TestingUser.ROLE,
                           self.user_access_token)
        self.assertTrue(data['error'] == 'You do not have permission to '
                                         'perform this operation')
