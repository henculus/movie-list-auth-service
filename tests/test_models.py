from flask_testing import TestCase
from app import app, db
from app.auth.models import User


class TestUserModel(TestCase):

    def create_app(self):
        app.config.from_object('app.config.TestingConfig')
        return app

    def test_uncode_auth_token(self):
        user = User(
            'test_fn',
            'test_sn',
            'test',
            '123',
            email='test@testmail.com'
        )
        db.session.add(user)
        db.session.commit()
        auth_token = user.encode_auth_token(user.id)
        self.assertTrue(isinstance(auth_token, bytes))

    def test_decode_auth_token(self):
        user = User(
            'test_fn',
            'test_sn',
            'test',
            '123',
            email='test@testmail.jp'
        )
        db.session.add(user)
        db.session.commit()
        auth_token = user.encode_auth_token(user.id)
        self.assertTrue(isinstance(auth_token, bytes))

        self.assertTrue(User.decode_auth_token(
            auth_token.decode("utf-8")) == 1)
