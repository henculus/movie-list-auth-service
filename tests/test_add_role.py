from tests.http_methods import add_role
from tests.base_test_case import BaseTestCase
from app.config import TestingUser

# TODO: Call role-require method to check correct work of "add role"
# TODO: Make roles list in models or in config


class TestAddRole(BaseTestCase):

    def test_add_role(self):
        data = add_role(self,
                        TestingUser.USER,
                        TestingUser.ROLE,
                        self.admin_access_token)
        self.assertTrue(data == 0)

    def test_add_non_existing_role(self):
        data = add_role(self,
                        TestingUser.USER,
                        "invalid_role",
                        self.admin_access_token)
        self.assertTrue(data['error'] == 'Role does not exists')

    def test_add_role_to_non_existing_user(self):
        data = add_role(self,
                        TestingUser.USER,
                        TestingUser.ROLE,
                        self.admin_access_token)
        self.assertTrue(data['error'] == 'User does not exists')

    def test_add_role_without_permissions(self):
        data = add_role(self,
                        TestingUser.USER,
                        TestingUser.ROLE,
                        self.user_access_token)
        self.assertTrue(data['error'] == 'You do not have permission to '
                                         'perform this operation')
