from tests.base_test_case import BaseTestCase
from tests.http_methods import refresh_access_token


class TestRefreshAccessToken(BaseTestCase):

    def test_refresh_access_token(self):
        data = refresh_access_token(self, self.user_refresh_token)
        self.assertTrue(data['error'] == 0)
        self.assertTrue(data['user_refresh_token'] != 0)

    def test_refresh_token_with_invalid_token(self):
        data = refresh_access_token(self, 'invalid_token')
        self.assertTrue(data['error'] == 'You do not have permission to '
                                         'perform this operation')
