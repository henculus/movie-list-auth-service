# Auth module HTTP methods
Description of HTTP methods provided by Auth module 

**Content:**
1. [Register](#register)
2. [Login](#login)
3. [Add Role](#add-role)
4. [Delete Role](#delete-role)
5. [Logout](#logout)
6. [Refresh Access Token](#refresh-access-token)
7. [Change Password](#change-password)
8. [Get Info](#get-info)

## Register
Creating new user and send JWT token in response

Return a JSON with error message in case of error

**Request:**
```
POST /register 
Content-Type: application/json
{
"username": "<username>",
"password": "<password>",
"first_name": "<first_name>",
"second_name": "<second_name>"
}
```
**Response:**
```
{
"access_token": "<access_token>",
"refresh_token": "<refresh_token>"
}
```

## Login
Accepts credentials and send JWT token in response

Return a JSON with error message in case of error

**Request:**
```
POST /login
Content-Type: application/json
{
"username": "<username>",
"password": "<password>"
}
```
**Response:**
```
{
"access_token": "<access_token>",
"refresh_token": "<refresh_token>"
}
```

## Add Role
Adding a role to a user  
Bearer should have rights to changing user roles

Return a JSON with error message in case of error

**Request:**
```
POST /add_role
Content-Type: application/json
Authorization: Bearer <access_token>
{
"username": "<username>",
"role": "<role>"
}
```

## Delete Role
Deleting a role from user  
Bearer should have rights to changing user roles

Return a JSON with error message in case of error

**Request:**
```
DELETE /delete_role
Content-Type: application/json
Authorization: Bearer <access_token>
{
"username": "<username>",
"role": "<role>"
}
```

## Logout
Deleting access and refresh token from server

**Request:**
```
GET /logout
Authorization: Bearer <access_token>
```

## Refresh Access Token
_**WARNING:** TEMPORARY, SIGNATURE MAY BE CHANGED, MAY NOT WORK_

Return a new JWT access token by providing a refresh token

Return a JSON with error message in case of error

**Request:**
```
GET /refresh
Authorization: Bearer <refresh_token>
```

## Change Password
Changing password for owner of token

Return a JSON with error message in case of error

**Request:**
```
POST /change_password
Content-Type: application/json
Authorization: Bearer <access_token>
{
"old_password": "<old_password>",
"new_password": "<new_password>"
}
```

## Get Info

Returning an information about user

Returning a JSON with error message in case of error

**Request:**
```
GET /user_info
Authorization: Bearer <access_token>
```  
**OR**

**Request:**
```
POST /user_info
Content-Type: application/json
Authorization: Bearer <access_token>
{
"username": "<username>"
}
```
_**WARNING:** NEW FIELDS WILL BE ADDED TO RESPONSE_

**Response:**
```
{
"username": "<username>",
"first_name": "<first_name>",
"second_name": "<second_name>",
"email": "<email>",
"profile_picture": "<href to pictire>",
...
}
```

