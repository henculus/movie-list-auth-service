import venv
import subprocess
import os
import sys
import logging


def main(args):
    """
    Creating a virtual environment in folder with name 'venv'
    :param args: useless for now, name of folder and other params may be added
    """
    builder = Builder(with_pip=True)
    builder.create('venv')


def call_in_env(context, command, args=None):
    """
    Call the command inside virtual environment
    :param context: contain meta information about environment
    :param command: name of command to run
    :param args: args for command
    :return: True if executed without errors, else False
    """
    env_command_path = os.path.join(context.bin_path, command)
    if os.path.exists(env_command_path):
        command = env_command_path
    else:
        logging.warning("Cannot find environment path: " + env_command_path)

    call_args = [command]
    if args is not None:
        call_args.extend(args)
        logging.debug(call_args)
    try:
        return subprocess.call(call_args) == 0
    except (subprocess.SubprocessError, OSError):
        return False


class Builder(venv.EnvBuilder):

    def __init__(self, system_site_packages=False, clear=False, symlinks=False,
                 upgrade=False, with_pip=False):
        """
        Constructor override
        :param system_site_packages: should env use system site packages
        :param clear: -
        :param symlinks: -
        :param upgrade: should env and packages be updated
        :param with_pip: should env set up pip
        """
        super().__init__(system_site_packages, clear, symlinks, upgrade,
                         with_pip)
        logging.info("Setting up the environment")

    def post_setup(self, context):
        """
        Callback override that called after setup a environment
        :param context: object contains meta data about environment
        :return:
        """
        logging.info("Updating pip")
        call_in_env(context,
                    'pip3.exe' if os.name == 'nt' else 'pip3',
                    ['install', '--upgrade', 'pip'])
        logging.info("Environment installed, installing requirements")
        call_in_env(context,
                    'pip3.exe' if os.name == 'nt' else 'pip3',
                    ['install', '-r', 'requirements.txt'])


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
